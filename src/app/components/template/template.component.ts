import { Component, OnInit } from '@angular/core';
import { InformacionI } from 'src/app/interfaces/informacion';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  enviado:boolean = false;

    datos: InformacionI = {
      correo:'',
      tramites:'',
      descripcion:''
  }
  constructor() { }

  ngOnInit(): void {
  }

  guardar():void {
    this.enviado = true;
    console.log("Mensaje Guardado");
    console.log(this.datos.correo);
    console.log(this.datos.tramites);
    console.log(this.datos.descripcion);
  }
}
